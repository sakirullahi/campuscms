CampusCMS
=========

A Content management system for college websites using Python  



REQUIREMENTS
============

- Python 2.7
- MongoDB
- Django 1.6
- Tastypie 0.4.5
- Angularjs


Installation
============

1. Install Python 2.7 if not installed 
2. Install and run MongoDB 

    
```
#!shell

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10

echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list   
sudo apt-get update   
sudo apt-get install -y mongodb-org    
sudo service mongod start // to start mongodb   
//sudo service mongod stop  // to stop mongdb   
//sudo service mongod restart // to stop mongodb
```
More info : [http://docs.mongodb.org/manual/tutorial/getting-started/](http://docs.mongodb.org/manual/tutorial/getting-started/) and [http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/

3. download basic mongodb db structure from https://bitbucket.org/sakirullahi/campuscms/downloads/campusCMSTest.tar.gz and extract to folder. cd to campusCMSTest folder

    
```
#!shell

run sudo mongorestore --db campusCMSTest --drop  campusCMSTest
```

4. Install Python Package Manager PIP
    
    
```
#!shell

sudo apt-get install python-pip // install pip    
sudo pip install -U pip // upgrade pip using itself    
```


5. Install Python Development Environment
    
    
```
#!shell

sudo pip install virtualenv  // install virtual env   
mkdir ~/virtualenvironment // create a isolated directory for ur app    
//virtualenv ~/virtualenvironment/my_new_app  //create a folder for your new app that includes a clean copy of Python    
//cd ~/virtualenvironment/my_new_app/bin    
//source activate //activate virtual env    
//deactivate // to exit virtualenv    

sudo pip install virtualenvwrapper //Install Virtualenvwrapper  
```
  
Add following lines to ~/.bashrc file

    
```
#!shell

export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh
```

run startup file :  
```
#!shell

source ~/.bashrc 
```

Download latest stable release from https://bitbucket.org/sakirullahi/campuscms/get/photo.zip and extract .
cd to campus cms app folder

    
```
#!shell

mkvirtualenv CampusCMS  // create virtualenv
workon
    
pip install -r requirements.txt
```


7.    
```
#!shell

pip install six
```

8.    
```
#!shell

pip install mimeparse
apt-get install libxml2-dev libxslt1-dev python-dev
```

9.    Download themes provided here [https://bitbucket.org/sakirullahi/campuscms/downloads]   
10.    Start Server : 
```
#!shell

python manage.py runserver
```

11   Locate to browser to [http://127.0.0.1:8000/admin/] to view admin panel.  Enter admin as username and password. Click on Template Manager >> Install Template. Select on of the theme you downloaded earlier. From the change template drop down list, Select theme u installed and click on change template. 
12.   Locate browser to [http://127.0.0.1:8000/]  to view client side


Cool......