from django.views.generic import TemplateView


class Login(TemplateView):
    template_name = 'admin/index.html'

    def get_context_data(self, **kwargs):
        context = super(Login, self).get_context_data(**kwargs)
        context.update({})
        return context

#
#
# from postApp.models import Post, Page, Block, News, Slide
# from django.shortcuts import render
#
#
# ## Control panel
# def cpanel(request):
#
#     ## fetching nav  list
#     nav_list = Page.objects.filter(status=1)
#
#     ## fetching college info
#     college = {}
#     college['name'] = Post.objects.get(title="college_name").body            # college name
#     college['contact'] = Post.objects.get(title="college_contact").body      # college contact
#
#
#     ## fetching slide list
#     slide_list = Slide.objects.all()
#
#     ## fetching news list
#     news_list = News.objects.all()
#
#     home_page = Page.objects.get(type="home")                               # reading id of home page
#     page_title = home_page.title                                            # reading id of home page
#     page_type = home_page.type                                                # reading id of page type
#     home_page_id = str(home_page.id)                                        # converting to string
#     post_list = Block.objects.filter(page_id=home_page_id)                  # fetching block entries with home page id
#     post_ids = []                                                           # initialise empty list
#
#     for post in post_list:
#         post_ids.append(str(post.post_id))                                  # storing id of each block entry to list
#
#     posts = Post.objects.filter(id__in=post_ids)                            # reading posts with ids in post_ids list
#
#     template_name = 'current/cpanel.html'
#     context = {'page_type': page_type, 'page_title': page_title, 'posts': posts, "nav_list": nav_list, "college": college, "slide_list": slide_list, "news_list": news_list}
#
#     return render(request, template_name, context)
#
