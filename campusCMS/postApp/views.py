from postApp.models import Post, Page, Block, News, Slide
from django.shortcuts import render


## Home Page
def index(request):

    ## fetching nav  list
    nav_list = Page.objects.filter(status=1)

    ## fetching college info
    college = {}
    college['name'] = Post.objects.get(title="college_name").body            # college name
    college['contact'] = Post.objects.get(title="college_contact").body      # college contact


    ## fetching slide list
    slide_list = Slide.objects.all()

    ## fetching news list
    news_list = News.objects.all()

    home_page = Page.objects.get(type="home")                               # reading id of home page
    page_title = home_page.title                                            # reading id of home page
    page_type = home_page.type                                                # reading id of page type
    home_page_id = str(home_page.id)                                        # converting to string
    post_list = Block.objects.filter(page_id=home_page_id)                  # fetching block entries with home page id
    post_ids = []                                                           # initialise empty list

    for post in post_list:
        post_ids.append(str(post.post_id))                                  # storing id of each block entry to list

    posts = Post.objects.filter(id__in=post_ids)                            # reading posts with ids in post_ids list

    template_name = 'current/index.html'
    context = {'page_type': page_type, 'page_title': page_title, 'posts': posts, "nav_list": nav_list, "college": college, "slide_list": slide_list, "news_list": news_list}

    return render(request, template_name, context)


## Other Pages
def pages(request, page_slug):

    ## fetching nav  list
    nav_list = Page.objects.filter(status=1)

    ## fetching college info
    college = {}
    college['name'] = Post.objects.get(title="college_name").body            # college name
    college['contact'] = Post.objects.get(title="college_contact").body      # college contact

    ## fetching slide list
    slide_list = Slide.objects.all()

    ## fetching news list
    news_list = News.objects.all()

    custom_page = Page.objects.get(slug=page_slug)                                  # reading id of custom_page
    page_title = custom_page.title                                              # reading id of page title
    page_type = custom_page.type                                                # reading id of page type
    custom_page_id = str(custom_page.id)                                        # converting to string
    post_list = Block.objects.filter(page_id=custom_page_id)                    # fetching block entries with home page id
    post_ids = []                                                               # initialise empty list

    for post in post_list:
        post_ids.append(str(post.post_id))                                      # storing id of each block entry to list

    posts = Post.objects.filter(id__in=post_ids)                                # reading posts with ids in post_ids list

    if custom_page.type == "home":
        template_name = 'current/index.html'
    elif custom_page.type == "about":
        template_name = 'current/about.html'
    elif custom_page.type == "gallery":
        template_name = 'current/gallery.html'
    elif custom_page.type == "contact":
        template_name = 'current/contact.html'
    else:
        template_name = 'current/other.html'



    context = {'page_type': page_type, 'page_title': page_title, 'posts': posts, "nav_list": nav_list, "college": college, "slide_list": slide_list, "news_list": news_list, "config": 1}

    return render(request, template_name, context)