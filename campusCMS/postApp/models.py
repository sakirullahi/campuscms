from mongoengine import *
from campusCMS.settings import DBNAME
from django.template.defaultfilters import slugify

## connecting to mongodb
connect(DBNAME)


class Post(Document):
    title = StringField()
    body = StringField()

    def __unicode__(self):
        return self.title


class Page(Document):
    title = StringField()
    nav_name = StringField()
    slug = StringField()            # slugify of page_title
    type = StringField()            # home, about, contact, gallery, other
    status = IntField()             # published or not
    pub_date = DateTimeField()
    updated_date = DateTimeField()

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        temp_slug = slugify(self.title)
        counter = 0

        while 1:
            if counter != 0:
                temp_slug += str(counter)
            result = Page.objects.filter(slug=temp_slug)
            if result:
                counter += 1
            else:
                self.slug = temp_slug
                break

        super(Page, self).save(*args, **kwargs)


class Block(Document):
    post_id = StringField()
    page_id = StringField()


class News(Document):
    body = StringField()
    pub_date = DateTimeField()


class Slide(Document):
    url = StringField()


class User(Document):
    username = StringField()
    password = StringField()