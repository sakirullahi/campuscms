from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authorization import Authorization
from postApp.models import Post, Page, Block, News, Slide, User
from tastypie_mongoengine import resources


class PostResource(resources.MongoEngineResource):
    class Meta:
        queryset = Post.objects.all()
        resource_name = 'post'
        authorization = Authorization()
        filtering = {
            'id': ALL
        }


class PageResource(resources.MongoEngineResource):
    class Meta:
        queryset = Page.objects.all()
        resource_name = 'page'
        authorization = Authorization()
        filtering = {
            'type': ALL
        }


class BlockResource(resources.MongoEngineResource):
    class Meta:
        queryset = Block.objects.all()
        resource_name = 'block'
        authorization = Authorization()
        filtering = {
            'page_id': ALL
        }


class NewsResource(resources.MongoEngineResource):
    class Meta:
        queryset = News.objects.all()
        resource_name = 'news'
        authorization = Authorization()


class SlideResource(resources.MongoEngineResource):
    class Meta:
        queryset = Slide.objects.all()
        resource_name = 'slide'
        authorization = Authorization()


class UserResource(resources.MongoEngineResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authorization = Authorization()
        filtering = {
            'username': ALL,
            'password': ALL
        }