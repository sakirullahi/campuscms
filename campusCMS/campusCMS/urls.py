from django.conf.urls import *
from tastypie.api import Api
from postApp.api.resources import PostResource, PageResource, BlockResource, UserResource
from postApp import views

## Registering resources with Api
v1_api = Api(api_name='v1')
v1_api.register(PageResource())
v1_api.register(PostResource())
v1_api.register(BlockResource())
v1_api.register(UserResource())


urlpatterns = patterns('',
    (r'^$', include('postApp.urls')),                               # User urls
    (r'^admin/', include('adminApp.urls')),                         # Admin urls
    url(r'^(?P<page_slug>\w+)/$', views.pages, name='pages'),       # custom page url
    (r'^api/', include(v1_api.urls)),                               # Api Urls
)
