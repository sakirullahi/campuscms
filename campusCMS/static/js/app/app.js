'use strict';

var adminApp = angular.module("adminApp", ['ui.tinymce'], function ($interpolateProvider) {
        $interpolateProvider.startSymbol("{[{");
        $interpolateProvider.endSymbol("}]}");
    }
);

//adminApp.run(function ($http, $cookies) {
//    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
//})




adminApp.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "../../static/js/app/views/login.html",
            controller: "CPanelCtrl",
            resolve: {
                callMe:function($rootScope){
//                    $rootScope.loginerror = true;


//                    alert("das")
                }
            }
        })

        .when("/cpanel", {
            templateUrl: "../../static/js/app/views/cpanel.html",
            controller: "CPanelCtrl",
            resolve: {
                callMe:function(){
                }
            }
        })

        .when("/postManager", {
            templateUrl: "../../static/js/app/views/postList.html",
            controller: "PostCtrl",
            resolve: {
                loadPosts:function(){
                }
            }
        })

        .when("/editPost", {
            templateUrl: "../../static/js/app/views/editPost.html",
            controller: "PostCtrl",
            resolve: {
                loadPosts:function($rootScope){
                    $rootScope.haveID = true;
                }
            }
        })

        .when("/addPost", {
            templateUrl: "../../static/js/app/views/editPost.html",
            controller: "PostCtrl",
            resolve: {
                loadPosts:function($rootScope){
                    $rootScope.haveID = null;
                    $rootScope.editPost = {};
                    $rootScope.editPost.title = null;
                    $rootScope.editPost.body = null;
                }
            }
        })

//        .when("/post/:id", {
//            templateUrl: "static/js/app/views/view.html",
//            controller: "PostController",
//            resolve: {
//                post: function ($route, PostService) {
//                    var postId = $route.current.params.id
//                    return PostService.get(postId);
//                }
//            }
//        })

        .otherwise({
            redirectTo: '/'
        })
})


