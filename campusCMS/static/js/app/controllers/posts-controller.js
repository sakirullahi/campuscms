

adminApp.controller('PostCtrl', function PostCtrl($scope, $log, $http, $location, $rootScope, $timeout, PostService) {


    $scope.postManager = function(){
        $location.path('postManager');

    };

    $scope.loadItems = function() {
        $scope.posts = $http.get('/api/v1/post/').then(function(response){
            return response.data.objects;
        });
    };

    $scope.loadItems();

    $scope.editThisPost = function(post){
        $rootScope.editPost = post;
        $location.path('/editPost');
    }

    $scope.updatePost = function(){
        PostService.update($rootScope.editPost).then(function (data) {
            $location.path('/cpanel');
            $rootScope.statusMsg = "Post Updated Successfully!"
            $rootScope.statusMsgType = "successMsg"
            $timeout(function(){
                $rootScope.statusMsg = null;
            },2000)
        });
    }

    $scope.addPost = function(){

        $log.log($scope.editPost.body);

//        PostService.save($rootScope.editPost).then(function(){
//            if($rootScope.editPost.title && $rootScope.editPost.body){
//                $location.path('/postManager');
//                $rootScope.statusMsg = "Post Added Successfully!"
//                $rootScope.statusMsgType = "successMsg"
//                $timeout(function(){
//                    $rootScope.statusMsg = null;
//                },2000)
//            }
//            else{
//                alert("Fill all Fields");
//            }
//        });
    }

    $scope.deletePost = function(post,index){

        PostService.del(post).then(function(){
//                $location.path('/cpanel');
                $rootScope.statusMsg = "Post Deleted Successfully!"
                $scope.loadItems();
                $rootScope.statusMsgType = "errorMsg"
                $timeout(function(){
                    $rootScope.statusMsg = null;
                },2000)
        });

    }



//
//    $scope.currentItem = {};
//    $scope.errors = {};
//
//    $scope.saveItem = function() {
//        ModelUtils.save('/api/v1/post/',$scope.currentItem, $scope.errors).then(function(){
//            $scope.loadItems();
//            $scope.currentItem = {};
//        });
//    };
//    $scope.updateItem = function() {
////        alert("das")
//        var item = $scope.currentItem;
//        ModelUtils.update('/api/v1/post/',item).then(function(){
//            $scope.loadItems();
//            $scope.currentItem = {};
//        });
//    };
//    $scope.editItem = function(item) {
//        $scope.currentItem = item;
//    };
//
//    $scope.delItem = function(item) {
//        ModelUtils.del('/api/v1/post/',item).then(function(){
//            $scope.loadItems();
//        });
//
//    };


});



//Blog.controller('PostController', function ($scope, $routeParams, $location, PostService, TagService, GlobalService, post) {
//    $scope.post = post;
//    $scope.globals = GlobalService;
//    var failureCb = function (status) {
//        console.log(status);
//    }
//    //options for modals
//    $scope.opts = {
//        backdropFade: true,
//        dialogFade: true
//    };
//    //open modals
//    $scope.open = function (action) {
//        if (action === 'edit'){
//            $scope.postModalEdit = true;
//        };
//    };
//    //close modals
//    $scope.close = function (action) {
//        if (action === 'edit'){
//            $scope.postModalEdit = false;
//        };
//    };
//    //calling board service
//    $scope.update = function () {
//        PostService.update($scope.post).then(function (data) {
//            $scope.post = data;
//            $scope.postModalEdit = false;
//        }, failureCb);
//    };
//    $scope.getTag = function (text) {
//        return TagService.query(text).then(function (data) {
//            return data;
//        }, function (status) {
//            console.log(status);
//        });
//    };
//    $scope.selectTag = function () {
//        if (typeof $scope.selectedTag === 'object') {
//            $scope.post.tags.push($scope.selectedTag.url);
//            $scope.post.tags_details.push($scope.selectedTag);
//            $scope.selectedTag = null;
//        }
//    };
//    $scope.removeTag = function (category) {
//        var index = $scope.post.tags_details.indexOf(category);
//        $scope.post.tags_details.splice(index, 1);
//        var index = $scope.post.tags.indexOf(category.url);
//        $scope.post.tags.splice(index, 1);
//    };
//});