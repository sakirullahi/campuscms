
adminApp.controller('CPanelCtrl', function CPanelCtrl($scope, $rootScope, $log, $http, $location, UserService) {

    $scope.initialize = function(data) {
        $scope.initData = data;
    };

    $scope.login = function() {
        $scope.items = $http.get('/api/v1/user/').then(function(response){
            return response.data.objects;
        });
    };

    $scope.login = function(user) {

        UserService.login(user);

    };

    $scope.logout = function() {
        UserService.logout();

    };

    $scope.preview = function() {

        window.open('../', '_blank' );

    };

    $scope.go = function(url){
        $location.path(url);
    }

});