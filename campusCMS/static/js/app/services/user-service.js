adminApp.factory('UserService', function($http, $log, $location, $rootScope) {

    var api_url = "/api/v1/user/";
    $rootScope.loginerror = null;


    var UserService = {

        login: function(user){
            $http.get(api_url+'?username='+user.username+'&password='+user.password).then(function(response){

                if(!response.data.objects.length){
//                    alert("Invalid Username or Password. Try again.")
                    $rootScope.loginerror = true;
                }
                else{
                    user.username = null;
                    user.password = null;
                    $rootScope.loginerror = null;
                    $location.path( '/cpanel');
                }

            });
        },

        logout: function(){
            $location.path('/');
        },

        get: function(url,id) {
            $http.get(url + id + '/').then(function(response){response.data});
        },
        create: function(url, obj, errors) {
            return $http.post(url, obj).
                success(function(response, status, headers, config) {
                    angular.extend(obj, response);
                }).
                error(function(response, status, headers, config) {
                    handleErrors(response, status, errors);
                });
        },
        save: function(url, obj, errors) {
            if (angular.isDefined(obj.id)) {
                return $http.put(url + obj.id + '/', obj).
                        success(function(response, status, headers, config) {
                            angular.extend(obj, response);
                        }).
                        error(function(response, status, headers, config) {
                            handleErrors(response, status, errors);
                        });
            } else {
                return this.create(url, obj, errors);
            }
        },
        del: function(url, obj) {
            return $http.delete(url + obj.id + '/');
        },
        update: function(url, obj){
            return $http.put(url + obj.id + '/', obj);
        }
    };
    return UserService;
});


//adminApp.factory('UserService', function ($http, $q) {
//    var api_url = "/user/";
//    return {
//        get: function (post_id) {
//            var url = api_url + post_id + "/";
//            var defer = $q.defer();
//            $http({method: 'GET', url: url}).
//                success(function (data, status, headers, config) {
//                    defer.resolve(data);
//                })
//                .error(function (data, status, headers, config) {
//                    defer.reject(status);
//                });
//            return defer.promise;
//        },
//        list: function () {
//            var defer = $q.defer();
//            $http({method: 'GET', url: api_url}).
//                success(function (data, status, headers, config) {
//                    defer.resolve(data);
//                }).error(function (data, status, headers, config) {
//                    defer.reject(status);
//                });
//            return defer.promise;
//        },
//        update: function (post) {
//            var url = api_url + post.id + "/";
//            var defer = $q.defer();
//            $http({method: 'PUT',
//                url: url,
//                data: post}).
//                success(function (data, status, headers, config) {
//                    defer.resolve(data);
//                }).error(function (data, status, headers, config) {
//                    defer.reject(status);
//                });
//            return defer.promise;
//        },
//        save: function (post) {
//            var url = api_url;
//            var defer = $q.defer();
//            $http({method: 'POST',
//                url: url,
//                data: post}).
//                success(function (data, status, headers, config) {
//                    defer.resolve(data);
//                }).error(function (data, status, headers, config) {
//                    defer.reject(status);
//                });
//            return defer.promise;
//        },
//    }
//});


